package gobjects

import "time"

type Message struct {
	Handle      string
	From        string
	DisplayName string
	Message     string
	Image       string
	FromMe      bool
	MessageID   string
	Timestamp   time.Time
	Acknowledged bool
	Error bool
}
