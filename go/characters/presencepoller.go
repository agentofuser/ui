package characters

import (
	"cwtch.im/cwtch/event"
	"cwtch.im/ui/go/cwutil"
	"cwtch.im/ui/go/gobjects"
	"cwtch.im/ui/go/the"
	"time"
)

func PresencePoller(getContact func(string) *gobjects.Contact, addContact func(contact *gobjects.Contact), updateContact func(string)) { // TODO: make this subscribe-able in ricochet
	time.Sleep(time.Second * 4)
	for {
		contacts := the.Peer.GetContacts()
		for i := range contacts {
			ct := getContact(contacts[i])
			if ct == nil { // new contact has attempted to connect with us, treat it as an invite
				toc := the.Peer.GetContact(contacts[i])
				c, _ := the.Peer.GetProfile().GetContact(contacts[i])
				deleted,_ := c.GetAttribute("deleted")

				if deleted != "deleted" {
					addContact(&gobjects.Contact{
						toc.Onion,
						toc.Name,
						cwutil.RandomProfileImage(toc.Onion),
						"",
						0,
						0,
						c.Trusted,
					})

					the.CwtchApp.EventBus().Publish(event.NewEvent(event.SetPeerAttribute, map[event.Field]string{
						event.RemotePeer: contacts[i],
						event.Key:        "name",
						event.Data:       c.Name,
					}))
				}
			}

			cxnState, found := the.Peer.GetPeers()[contacts[i]]
			if !found {
				the.Peer.PeerWithOnion(contacts[i])
				c2 := getContact(contacts[i])
				if c2 != nil && c2.Status != -2 {
					c2.Status = -2
					updateContact(contacts[i])
				}
			} else {
				c2 := getContact(contacts[i])
				if c2 != nil && c2.Status != int(cxnState) {
					c2.Status = int(cxnState)
					updateContact(contacts[i])
				}
			}
		}
		time.Sleep(time.Second * 4)
	}
}
