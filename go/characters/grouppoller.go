package characters

import (
	"cwtch.im/ui/go/gobjects"
	"cwtch.im/ui/go/the"
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
	"time"
)

func GroupPoller(getContact func(string) *gobjects.Contact, updateContact func(string)) {
	for {
		time.Sleep(time.Second * 4)

		servers := the.Peer.GetServers()
		groups := the.Peer.GetGroups()
		for i := range groups {
			group := the.Peer.GetGroup(groups[i])
			if group != nil && group.GroupID != "" {
				deleted,_ := group.GetAttribute("deleted")
				if deleted != "deleted" {
					getContact(group.GroupID).Status = int(servers[group.GroupServer])
					updateContact(group.GroupID)
				}
			} else {
				log.Errorf("grouppoller found a group that is nil :/")
			}
		}
	}
}
