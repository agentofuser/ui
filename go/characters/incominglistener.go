package characters

import (
	"cwtch.im/cwtch/event"
	"cwtch.im/ui/go/cwutil"
	"cwtch.im/ui/go/gobjects"
	"cwtch.im/ui/go/the"
	"encoding/base32"
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
	"strings"
	"time"
)

func IncomingListener(callback func(*gobjects.Message), groupErrorCallback func(string, string,string)) {
	q := event.NewEventQueue(1000)
	the.CwtchApp.EventBus().Subscribe(event.NewMessageFromPeer, q.EventChannel)
	the.CwtchApp.EventBus().Subscribe(event.NewMessageFromGroup, q.EventChannel)
	the.CwtchApp.EventBus().Subscribe(event.NewGroupInvite, q.EventChannel)
	the.CwtchApp.EventBus().Subscribe(event.SendMessageToGroupError, q.EventChannel)
	the.CwtchApp.EventBus().Subscribe(event.SendMessageToPeerError, q.EventChannel)

	for {
		e := q.Next()

		switch e.EventType {
		case event.NewMessageFromPeer://event.TimestampReceived, event.RemotePeer, event.Data
			ts, _ := time.Parse(time.RFC3339Nano, e.Data[event.TimestampReceived])
			callback(&gobjects.Message{
				Handle:    e.Data[event.RemotePeer],
				From:      e.Data[event.RemotePeer],
				Message:   e.Data[event.Data],
				Image:     cwutil.RandomProfileImage(e.Data[event.RemotePeer]),
				Timestamp: ts,
			})
			if the.Peer.GetContact(e.Data[event.RemotePeer]) == nil {
				decodedPub, _ := base32.StdEncoding.DecodeString(strings.ToUpper(e.Data[event.RemotePeer]))
				the.Peer.AddContact(e.Data[event.RemotePeer], e.Data[event.RemotePeer], decodedPub, false)
			}
			the.Peer.PeerWithOnion(e.Data[event.RemotePeer])
			if e.Data[event.Data] != "ack" {
				the.Peer.SendMessageToPeer(e.Data[event.RemotePeer], "ack")
			}
		case event.NewMessageFromGroup://event.TimestampReceived, event.TimestampSent, event.Data, event.GroupID, event.RemotePeer
			var name string
			var exists bool
			ctc := the.Peer.GetContact(e.Data[event.RemotePeer])
			if ctc != nil {
				name, exists = ctc.GetAttribute("nick")
				if !exists || name == "" {
					name = e.Data[event.RemotePeer] + "..."
				}
			} else {
				name = e.Data[event.RemotePeer] + "..."
			}

			ts, _ := time.Parse(time.RFC3339Nano, e.Data[event.TimestampSent])
			callback(&gobjects.Message{
				MessageID: e.Data[event.Signature],
				Handle:    e.Data[event.GroupID],
				From:      e.Data[event.RemotePeer],
				Message:   e.Data[event.Data],
				Image:     cwutil.RandomProfileImage(e.Data[event.RemotePeer]),
				FromMe:    e.Data[event.RemotePeer] == the.Peer.GetProfile().Onion,
				Timestamp: ts,
				Acknowledged: true,
				DisplayName: name,
			})
		case event.NewGroupInvite:
			log.Debugf("got a group invite!")
		case event.SendMessageToGroupError:
			groupErrorCallback(e.Data[event.GroupServer], e.Data[event.Signature], e.Data[event.Error])
		case event.SendMessageToPeerError:
			groupErrorCallback(e.Data[event.RemotePeer], e.Data[event.Signature], e.Data[event.Error])
		}
	}
}