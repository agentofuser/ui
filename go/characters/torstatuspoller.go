package characters

import (
	"git.openprivacy.ca/openprivacy/libricochet-go/connectivity"
	"time"
)

func TorStatusPoller(setTorStatus func(int, string), acn connectivity.ACN) {
	for {
		time.Sleep(time.Second)

		percent, message := acn.GetBootstrapStatus()
		var statuscode int
		if percent == 0 {
			statuscode = 0
			message = "can't find tor. is it running? is the controlport configured?"
		} else if percent == 100 {
			statuscode = 3
			message = "tor appears to be running just fine!"
		} else if percent < 80 {
			statuscode = 1
		} else {
			statuscode = 2
		}

		setTorStatus(statuscode, message)
	}
}
