package the

import (
	"cwtch.im/cwtch/app"
	libPeer "cwtch.im/cwtch/peer"
)

var CwtchApp app.Application
var Peer libPeer.CwtchPeer
var CwtchDir string

type AckId struct {
	ID string
	Ack bool
	Error bool
}

var AcknowledgementIDs map[string][]*AckId
