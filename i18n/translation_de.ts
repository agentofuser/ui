<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AddGroupPane</name>
    <message>
        <location filename="../qml/panes/AddGroupPane.qml" line="17"/>
        <source>create-group-title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/AddGroupPane.qml" line="27"/>
        <source>server-label</source>
        <extracomment>Server label</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/AddGroupPane.qml" line="38"/>
        <source>group-name-label</source>
        <extracomment>Group name label</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/AddGroupPane.qml" line="45"/>
        <source>default-group-name</source>
        <extracomment>default suggested group name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/AddGroupPane.qml" line="50"/>
        <source>create-group-btn</source>
        <extracomment>create group button</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BulletinOverlay</name>
    <message>
        <location filename="../qml/overlays/BulletinOverlay.qml" line="181"/>
        <source>new-bulletin-label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/overlays/BulletinOverlay.qml" line="193"/>
        <source>post-new-bulletin-label</source>
        <extracomment>Post a new Bulletin Post</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/overlays/BulletinOverlay.qml" line="199"/>
        <source>title-placeholder</source>
        <extracomment>title place holder text</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GroupSettingsPane</name>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="30"/>
        <source>server-label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="41"/>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="62"/>
        <source>copy-btn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="51"/>
        <source>invitation-label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="72"/>
        <source>group-name-label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="81"/>
        <source>save-btn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="91"/>
        <source>invite-to-group-label</source>
        <extracomment>Invite someone to the group</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="102"/>
        <source>invite-btn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="111"/>
        <source>delete-btn</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListOverlay</name>
    <message>
        <location filename="../qml/overlays/ListOverlay.qml" line="162"/>
        <source>add-list-item</source>
        <extracomment>Add a New List Item</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/overlays/ListOverlay.qml" line="174"/>
        <source>add-new-item</source>
        <extracomment>Add a new item to the list</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/overlays/ListOverlay.qml" line="180"/>
        <source>todo-placeholder</source>
        <extracomment>Todo... placeholder text</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MembershipOverlay</name>
    <message>
        <location filename="../qml/overlays/MembershipOverlay.qml" line="21"/>
        <source>membership-description</source>
        <extracomment>Below is a list of users who have sent messages to the group. This list may not reflect all users who have access to the group.</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="../qml/widgets/Message.qml" line="55"/>
        <source>dm-tooltip</source>
        <extracomment>Click to DM</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/widgets/Message.qml" line="154"/>
        <source>could-not-send-msg-error</source>
        <extracomment>Could not send this message</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/widgets/Message.qml" line="154"/>
        <source>acknowledged-label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/widgets/Message.qml" line="154"/>
        <source>pending-label</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyProfile</name>
    <message>
        <location filename="../qml/widgets/MyProfile.qml" line="169"/>
        <source>copy-btn</source>
        <extracomment>Button for copying profile onion address to clipboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/widgets/MyProfile.qml" line="173"/>
        <source>copied-clipboard-notification</source>
        <extracomment>Copied to clipboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/widgets/MyProfile.qml" line="203"/>
        <source>new-group-btn</source>
        <extracomment>create new group button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/widgets/MyProfile.qml" line="213"/>
        <source>paste-address-to-add-contact</source>
        <extracomment>ex: &quot;... paste an address here to add a contact ...&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OverlayPane</name>
    <message>
        <location filename="../qml/panes/OverlayPane.qml" line="44"/>
        <source>accept-group-invite-label</source>
        <extracomment>Do you want to accept the invitation to $GROUP</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/OverlayPane.qml" line="49"/>
        <source>accept-group-btn</source>
        <extracomment>Accept group invite button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/OverlayPane.qml" line="59"/>
        <source>reject-group-btn</source>
        <extracomment>Reject Group invite button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/OverlayPane.qml" line="73"/>
        <source>chat-btn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/OverlayPane.qml" line="80"/>
        <source>lists-btn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/OverlayPane.qml" line="87"/>
        <source>bulletins-btn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/OverlayPane.qml" line="94"/>
        <source>puzzle-game-btn</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PeerSettingsPane</name>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="29"/>
        <source>address-label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="40"/>
        <source>copy-btn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="44"/>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="51"/>
        <source>display-name-label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="60"/>
        <source>save-btn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="71"/>
        <source>delete-btn</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPane</name>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="17"/>
        <source>cwtch-settings-title</source>
        <extracomment>Cwtch Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="28"/>
        <source>zoom-label</source>
        <extracomment>Interface zoom (mostly affects text and button sizes)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="44"/>
        <source>large-text-label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="50"/>
        <source>default-scaling-text</source>
        <extracomment>&quot;Default size text (scale factor: &quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="54"/>
        <source>small-text-label</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
