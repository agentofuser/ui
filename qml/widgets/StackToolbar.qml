import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

import "controls" as Awesome
import "../fonts/Twemoji.js" as T

Rectangle { // OVERHEAD BAR ON STACK PANE
	id: toolbar
	anchors.left: parent.left
	anchors.right: parent.right
	anchors.top: parent.top
	height: 20 * gcd.themeScale + 4
	Layout.minimumHeight: height
	Layout.maximumHeight: height
	color: "#EDEDED"

	property alias text: lbl.text
	property alias aux: btnAux
	property alias back: btnBack
	property alias membership: btnMembership


	SimpleButton {// BACK BUTTON
		id: btnBack
		icon: "solid/arrow-circle-left"
		anchors.left: parent.left
		anchors.verticalCenter: parent.verticalCenter
		anchors.leftMargin: 6
		onClicked: theStack.pane = theStack.emptyPane
	}

	ScalingLabel { // TEXT
		id: lbl
		text: "open privacy exec"
		anchors.horizontalCenter: parent.horizontalCenter
		anchors.verticalCenter: parent.verticalCenter
	}

	RowLayout {

		anchors.right: parent.right
		anchors.rightMargin: 6
		anchors.verticalCenter: parent.verticalCenter

		SimpleButton { // Membership Button
			id: btnMembership
			icon: "solid/users"
			tooltip: "View Group Membership"
		}

		SimpleButton { // COG BUTTON
			id: btnAux
			icon: "solid/cog"
		}
	}
}