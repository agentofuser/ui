import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3


ColumnLayout {
	id: root
	width: parent.width

	property alias text: lbl.text
	signal updated


	Text { // DISPLAY THE TEXT IN READONLY MODE
		id: lbl
		text: root.text
		fontSizeMode: Text.HorizontalFit
		font.pixelSize: 36
		minimumPixelSize: 8
		Layout.minimumWidth: parent.width
		Layout.maximumWidth: parent.width
		width: parent.width / 2
		horizontalAlignment: Text.AlignHCenter
		textFormat: Text.PlainText


		MouseArea {
			anchors.fill: lbl

			onClicked: {
				lbl.visible = false
				txt.visible = true
				txt.text = lbl.text
				txt.selectAll()
				txt.focus = true
			}
		}
	}

	TextEdit { // MAKE IT AN EDITOR WHEN EDITING
		id: txt
		text: root.text
		visible: false
		selectByMouse: true
		Layout.minimumWidth: parent.width
		Layout.maximumWidth: parent.width
		font.pixelSize: lbl.font.pixelSize
		horizontalAlignment: Text.AlignHCenter

		Keys.onReturnPressed: {
			if (event.modifiers == Qt.NoModifier) {
				root.text = txt.text
				txt.visible = false
				lbl.visible = true
				root.updated(txt.text)
			}
		}
	}
}