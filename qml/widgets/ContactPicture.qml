import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import CustomQmlTypes 1.0

Item {
	id: imgProfile
	implicitWidth: 48
	implicitHeight: 48
	anchors.margins: 5

	property string handle
	property alias source: img.source
	property int status
	property bool isGroup
	property bool showStatus
	property bool highlight

	Rectangle {
		id: mainImage
		width: 48
		height: 48
		color:  highlight ? windowItem.cwtch_dark_color: "#FFFFFF"
		radius: width / 2

		Rectangle {
			width: highlight ? 44 : 48
			height: highlight ? 44 : 48
			color:  "#FFFFFF"
			radius: width / 2
			anchors.centerIn:parent


			Image { // PROFILE IMAGE
				id: img
				anchors.fill: parent
				fillMode: Image.PreserveAspectFit
				visible: false
			}

			Image { // CIRCLE MASK
				id: mask
				fillMode: Image.PreserveAspectFit
				visible: false
				source: "qrc:/qml/images/extra/clipcircle.png"
			}

			OpacityMask {
				anchors.fill: img
				source: img
				maskSource: mask
			}

		}

		Rectangle { // PRESENCE INDICATOR
					visible: showStatus
					color: "#FFFFFF"
					width: 8
					height: 8
					radius: 2
					anchors.right: parent.right
					anchors.bottom: parent.bottom
					anchors.margins: 4


					Rectangle { //-2:WtfCodeError,-1:Untrusted,0:Disconnected,1:Connecting,2:Connected,3:Authenticated,4:Failed,5:Killed
						color: status == 3 ? "green" : status == -1 ? "blue" : status == 1 ? "orange" : status == 2 ? "orange" : "red"
						width: 5
						height: 5
						radius: 2
						anchors.right: parent.right
						anchors.bottom: parent.bottom
						anchors.margins: 1.5
					}
				}

	}
}