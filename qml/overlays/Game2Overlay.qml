import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

import "../widgets"
import "../widgets/controls" as Awesome
import "../fonts/Twemoji.js" as T

ColumnLayout {
	Layout.fillWidth: true


	ScalingLabel {
	    text: "rock paper scissors. or chess? basically the same thing"
	}
}