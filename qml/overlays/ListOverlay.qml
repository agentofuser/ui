import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import "../widgets"
import "../widgets/controls" as Awesome
import "../fonts/Twemoji.js" as T
import "../utils.js" as Utils
import "../styles"

ColumnLayout {
	Layout.fillWidth: true
	width:parent.width

	Text {
		Layout.fillWidth: true
	}

	TextField {
		id: filter

		placeholderText: "Search.."

		style: CwtchTextFieldStyle{}

		anchors.left: parent.left
		anchors.right: parent.right

		anchors.margins: 10

		onTextChanged: {
		   bulletinView.filter = text
		   if (bulletinView.model.get(bulletinView.currentIndex).title.indexOf(text) == -1) {
				bulletinView.currentIndex = -1
		   }
		 }
	}

	Flickable { // THE MESSAGE LIST ITSELF
		id: sv
		clip: true
		Layout.alignment: Qt.AlignLeft | Qt.AlignTop
		Layout.fillHeight: true
		Layout.fillWidth: true
		contentWidth: parent.width
		contentHeight: parent.height
		boundsBehavior: Flickable.StopAtBounds
		maximumFlickVelocity: 800


		Connections {
			target: gcd

			onClearMessages: function() {
				jsonModel4.clear()
			}

			onAppendMessage: function(handle, from, displayName, message, image, mid, fromMe, ts, ack, error) {
				var msg
				try {
					msg = JSON.parse(message)
				} catch (e) {
					return
				}
				if (msg.o != 4) return

			   	if (msg.t != undefined) {
					jsonModel4.insert(0,{
						"title":msg.t,
						"selected":false,
						"from": from,
						"displayName": displayName,
						"timestamp": ts,
						"complete": false
						})
				}

				if(msg.c != undefined) {
					jsonModel4.get(msg.c).complete = true
				}

				if (sv.contentY + sv.height >= sv.contentHeight - colMessages.height && sv.contentHeight > sv.height) {
					sv.contentY = sv.contentHeight - sv.height
				}
			}
		}

		ScrollBar.vertical: ScrollBar{
			policy: ScrollBar.AlwaysOn
		}

		ListView {
			id: bulletinView
			anchors.left: parent.left
			anchors.leftMargin: 10
			anchors.topMargin: 10

			width: parent.width - 50
			height: parent.height - 20
			orientation: Qt.Vertical
			spacing: 10
			model: jsonModel4
			property string filter: ""
			delegate:

				Item {
					width: parent.width
					height:  title.indexOf(bulletinView.filter) >= 0 ? texttitle.height : 0
					visible: title.indexOf(bulletinView.filter) >= 0

					Column {
						width: parent.width

						RowLayout {
							CheckBox {
									checked: complete
									onClicked: {
										var msg = JSON.stringify({"o":4, "c":index})
										gcd.sendMessage(msg, btnSend.nextMessageID++)
									}
							}

							RowLayout {
								Text {
									id: texttitle
									text: '<b>' + Utils.htmlEscaped(title) + '</b> by ' + from + "<br/>" + timestamp
									leftPadding: 10
									topPadding: 5
									bottomPadding:5
									color: windowItem.cwtch_dark_color
								}
							}
						}


						Rectangle {
							height: 1
							color: windowItem.cwtch_color
							anchors {
								left: parent.left
								right: parent.right
							}
						}


					}

			   }

				focus: true
				ListModel {
					id: jsonModel4
				}
		}
	}

	GroupBox {
	    //: Add a New List Item
		title: qsTr("add-list-item")
		Layout.fillWidth: true

		RowLayout {
			Layout.fillWidth: true
			width: parent.width
			ColumnLayout {
				Layout.fillWidth: true


				Text {
				    //: Add a new item to the list
					text: qsTr("add-new-item")
				}

				TextField {
					id: newposttitle
					//: Todo... placeholder text
					placeholderText: qsTr("todo-placeholder")
					Layout.fillWidth: true
					style: CwtchTextFieldStyle{}
				}

				SimpleButton { // SEND MESSAGE BUTTON
					id: btnSend
					icon: "regular/paper-plane"
					text: "add"
					anchors.right: parent.right
					anchors.rightMargin: 2

					property int nextMessageID: 1

					onClicked: {
						if (newposttitle.text != "") {
							var msg = JSON.stringify({"o":4, "t":newposttitle.text})
							gcd.sendMessage(msg, nextMessageID++)
						}
						newposttitle.text = ""
					}
				}
			}
		}

	}






}
