import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import "../widgets"
import "../widgets/controls" as Awesome
import "../fonts/Twemoji.js" as T
import "../utils.js" as Utils
import "../styles"

ColumnLayout {
	Text {
		wrapMode: Text.Wrap
		Layout.maximumWidth: parent.width
		horizontalAlignment: Text.AlignHCenter
		padding: 1
		//: Below is a list of users who have sent messages to the group. This list may not reflect all users who have access to the group.
		text: qsTr("membership-description")
	}

	Flickable { // THE ACTUAL CONTACT LIST
		id: sv
		//Layout.alignment: Qt.AlignLeft | Qt.AlignTop
		clip: true
		Layout.minimumHeight: 100
		//Layout.maximumHeight: parent.height - 30
		Layout.fillHeight: true
		Layout.minimumWidth: parent.width
		Layout.maximumWidth: parent.width
		contentWidth: colContacts.width
		contentHeight: colContacts.height
		boundsBehavior: Flickable.StopAtBounds
		maximumFlickVelocity: 400


		ScrollBar.vertical: ScrollBar {
			policy: ScrollBar.AlwaysOn
		}

        ColumnLayout {
			id: colContacts
			width: sv.width
			spacing: 0

			Connections { // ADD/REMOVE CONTACT ENTRIES
				target: gcd

				onClearMessages: function() {
					contactsModel.clear()
				}

				onAppendMessage: function(handle, from, displayName, message, image, mid, fromMe, ts, ack, error) {
					var msg
					try {
						msg = JSON.parse(message)
					} catch (e) {
						return
					}

					if (from == "me") {
						return
					}

					for(var i = 0; i<contactsModel.count;i++){
						if(contactsModel.get(i)["_handle"] == handle) {
							return
						}
					}

					if (fromMe) {
						contactsModel.append({
							"_handle": handle,
							"_displayName": "me",
							"_image": image,
						})
					} else {
						contactsModel.append({
							"_handle": handle,
							"_displayName": displayName == "" ? handle : displayName,
							"_image": image,
						})
					}
				}
			}

			ListModel { // CONTACT OBJECTS ARE STORED HERE ...
				id: contactsModel
			}

			Repeater {
				model: contactsModel // ... AND DISPLAYED HERE
				delegate: ContactRow {
					handle: _handle
					displayName: _displayName
					image: _image
					trusted: true
					background: false
				}
			}
		}
	}
}
