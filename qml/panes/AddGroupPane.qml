import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4

import "../widgets"
import "../styles"

ColumnLayout { // settingsPane
	anchors.fill: parent


	StackToolbar {
		text: qsTr("create-group-title")
		aux.visible: false
	}

    Column {
    leftPadding: 10
    spacing: 5

	ScalingLabel {
	    //: Server label
		text: qsTr("server-label") + ":"
	}

	TextField {
		id: txtServer
		style: CwtchTextFieldStyle{ width: 400 }
		text: "2c3kmoobnyghj2zw6pwv7d57yzld753auo3ugauezzpvfak3ahc4bdyd"
	}

	ScalingLabel{
	    //: Group name label
		text: qsTr("group-name-label") + ":"
	}

	TextField {
		id: txtGroupName
		style: CwtchTextFieldStyle{ width: 400 }
		//: default suggested group name
		text: qsTr("default-group-name")
	}

	SimpleButton {
	    //: create group button
		text: qsTr("create-group-btn")

		onClicked: {
			gcd.createGroup(txtServer.text, txtGroupName.text)
		}
	}

    }//end of column with padding
}