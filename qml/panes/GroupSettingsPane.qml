import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4

import "../widgets"
import "../styles"
import "../utils.js" as Utils

ColumnLayout { // groupSettingsPane
	id: gsp
	anchors.fill: parent
	property string groupID
    property variant addrbook

	StackToolbar {
		id: toolbar
		aux.visible: false
		back.onClicked: theStack.pane = theStack.messagePane
	}

    Column {
    leftPadding: 10
    spacing: 5

	ScalingLabel {
		text: qsTr("server-label") + ":"
	}

	TextField {
		id: txtServer
		style: CwtchTextFieldStyle{ width: 400 }
		readOnly: true
	}

	SimpleButton {
		icon: "regular/clipboard"
		text: qsTr("copy-btn")

		onClicked: {
			gcd.popup("copied-clipboard-notification")
			txtServer.selectAll()
			txtServer.copy()
		}
	}

	ScalingLabel {
		text: qsTr("invitation-label") + ":"
	}

	TextField {
		id: txtInvitation
		style: CwtchTextFieldStyle{ width: 400 }
		readOnly: true
	}

	SimpleButton {
		icon: "regular/clipboard"
		text: qsTr("copy-btn")

		onClicked: {
			gcd.popup("copied-clipboard-notification")
			txtInvitation.selectAll()
			txtInvitation.copy()
		}
	}

	ScalingLabel{
		text: qsTr("group-name-label") + ":"
	}

	TextField {
		id: txtGroupName
		style: CwtchTextFieldStyle{ width: 400 }
	}

    SimpleButton {
        text: qsTr("save-btn")

        onClicked: {
            gcd.saveGroupSettings(groupID, txtGroupName.text)
            theStack.title = txtGroupName.text
            theStack.pane = theStack.messagePane
        }
    }

    //: Invite someone to the group
    ScalingLabel { text: qsTr("invite-to-group-label") }

    ComboBox {
        id: cbInvite
        //popup.font.pixelSize: 12
        width: 200
        //font.pixelSize: 20
        style: CwtchComboBoxStyle{}
    }

    SimpleButton {
        text: qsTr("invite-btn")

        onClicked: {
            gcd.inviteToGroup(addrbook[cbInvite.currentIndex], groupID)
        }
    }

    	SimpleButton {
    		icon: "regular/trash-alt"
    		text: qsTr("delete-btn")

    		onClicked: {
    			gcd.leaveGroup(groupID)
    			theStack.pane = theStack.emptyPane
    		}
    	}

    }//end of column with padding

	Connections {
		target: gcd

		onSupplyGroupSettings: function(gid, name, server, invite, accepted, addrbooknames, addrbookaddrs) {
			console.log("Supplied " + gid + " " + name)
		    gsp.groupID = gid
			toolbar.text = name
			txtGroupName.text = name
			txtServer.text = server
			txtInvitation.text = invite
			cbInvite.model = addrbooknames.map(function(e){return Utils.htmlEscaped(e)})
			addrbook = addrbookaddrs
		}
	}
}