import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4

import "../widgets"

ColumnLayout { // settingsPane
	anchors.fill: parent


	StackToolbar {
        //: Cwtch Settings title
		text: qsTr("cwtch-settings-title")
		aux.visible: false
	}

    Column {
    leftPadding: 10
    spacing: 5

	ScalingLabel {
		Layout.maximumWidth: parent.width
		//: Interface zoom (mostly affects text and button sizes)
		text: qsTr("zoom-label") + ":"
	}

	Slider {
		id: zoomSlider
		minimumValue: 0.5
		maximumValue: 4.0
		value: gcd.themeScale
		updateValueWhileDragging: false
		onValueChanged: {
		    gcd.themeScale = zoomSlider.value
		}
		width: 400
	}

	ScalingLabel {
		text: qsTr("large-text-label")
		size: 20
	}

	ScalingLabel{
	    //: "Default size text (scale factor: "
		text: qsTr("default-scaling-text") + zoomSlider.value + ")"
	}

	ScalingLabel {
		text: qsTr("small-text-label")
		size: 8
	}

    }//end of column with padding

	Component.onCompleted: {
		zoomSlider.value = gcd.themeScale //Screen.pixelDensity / 3.2 // artistic license. set by erinn. fight me before changing
		if (zoomSlider.value < zoomSlider.from) zoomSlider.value = zoomSlider.from
		if (zoomSlider.value > zoomSlider.to) zoomSlider.value = zoomSlider.to
	}
}