import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

import "../widgets"
import "../overlays"

ColumnLayout {
	Layout.fillWidth: true
	property alias title: toolbar.text
	id: overlay
	property string name
	property bool accepted
	property bool inGroup


	StackToolbar {
		id: toolbar

		membership.visible: gcd.currentOpenConversation.length == 32

		membership.onClicked: overlayStack.overlay = overlayStack.membershipOverlay

		aux.onClicked: {
			if (gcd.currentOpenConversation.length == 32) {
			    theStack.pane = theStack.groupProfilePane
			    gcd.requestGroupSettings(gcd.currentOpenConversation)
			} else {
			    theStack.pane = theStack.userProfilePane
			    gcd.requestPeerSettings()
			}
		}
		back.visible: true
	}

	RowLayout {
		visible:!overlay.accepted && (gcd.currentOpenConversation.length == 32)


		Text {
		    //: Do you want to accept the invitation to $GROUP
			text: qsTr("accept-group-invite-label") + " " + overlay.name + "?"
		}

		SimpleButton {
		    //: Accept group invite button
			text: qsTr("accept-group-btn")
			icon: "regular/heart"
			onClicked: {
				gcd.acceptGroup(gcd.currentOpenConversation)
				gcd.requestGroupSettings(gcd.currentOpenConversation)
			}
		}

		SimpleButton {
		    //: Reject Group invite button
			text: qsTr("reject-group-btn")
			icon: "regular/trash-alt"
			onClicked: {
    			gcd.leaveGroup(gcd.currentOpenConversation)
    			theStack.pane = theStack.emptyPane
			}
		}
	}

    RowLayout {
       id: switcher


       SimpleButton {
            text: qsTr("chat-btn")


            onClicked: overlayStack.overlay = overlayStack.chatOverlay
        }

        SimpleButton {
            text: qsTr("lists-btn")


            onClicked: overlayStack.overlay = overlayStack.listOverlay
        }

        SimpleButton {
            text: qsTr("bulletins-btn")


            onClicked: overlayStack.overlay = overlayStack.bulletinOverlay
        }

        SimpleButton {
            text: qsTr("puzzle-game-btn")


            onClicked: overlayStack.overlay = overlayStack.game1Overlay
        }

    }

    StackLayout {
        id: overlayStack
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: switcher.bottom
        implicitHeight: height
        currentIndex: 0

        property alias overlay: overlayStack.currentIndex
        readonly property int chatOverlay: 0
        readonly property int listOverlay: 1
        readonly property int bulletinOverlay: 2
        readonly property int game1Overlay: 3
        readonly property int membershipOverlay: 4


        ChatOverlay { //0
            Layout.maximumHeight: overlayStack.height
            Layout.maximumWidth: overlayStack.width
        }

        ListOverlay{ //1
            Layout.maximumHeight: overlayStack.height
            Layout.maximumWidth: overlayStack.width
        }

        BulletinOverlay{ //2
            Layout.maximumHeight: overlayStack.height
            Layout.maximumWidth: overlayStack.width
        }

        Game1Overlay{ //3
            Layout.maximumHeight: overlayStack.height
            Layout.maximumWidth: overlayStack.width
        }

        MembershipOverlay { //4
			Layout.maximumHeight: overlayStack.height
			Layout.maximumWidth: overlayStack.width
        }
    }

    Connections {
        target: gcd

		onResetMessagePane: function() {
			 overlayStack.overlay = overlayStack.chatOverlay
			 overlay.inGroup = false
		}

        onSupplyGroupSettings: function(gid, name, server, invite, accepted, addrbooknames, addrbookaddrs) {
            console.log("Supplied " + gid + " " + name + "Accepted " + accepted)
            overlay.name = name
            overlay.accepted = accepted
            overlay.inGroup = true
        }
    }
}