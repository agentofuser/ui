import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4

import "../widgets"
import "../styles"

ColumnLayout { // peerSettingsPane
	anchors.fill: parent


	StackToolbar {
	    id: toolbar
		aux.visible: false


		back.onClicked: theStack.pane = theStack.messagePane
	}

    Column {
    leftPadding: 10
    spacing: 5

	ScalingLabel {
		text: qsTr("address-label")
	}

	TextField {
		id: txtOnion
		style: CwtchTextFieldStyle{ width: 400 }
		readOnly: true
	}

	SimpleButton {
		icon: "regular/clipboard"
		text: qsTr("copy-btn")

		onClicked: {
		    //: notification: copied to clipboard
			gcd.popup(qsTr("copied-to-clipboard-notification"))
			txtOnion.selectAll()
			txtOnion.copy()
		}
	}

	ScalingLabel{
		text: qsTr("display-name-label")
	}

	TextField {
		id: txtDisplayName
		style: CwtchTextFieldStyle{ width: 400 }
	}

	SimpleButton {
	    text: qsTr("save-btn")

	    onClicked: {
	        gcd.savePeerSettings(txtOnion.text, txtDisplayName.text)
	        theStack.title = txtDisplayName.text
	        theStack.pane = theStack.messagePane
	    }
	}

	SimpleButton {
		icon: "regular/trash-alt"
		text: qsTr("delete-btn")

		onClicked: {
			gcd.setAttribute(txtOnion.text, "deleted", "deleted")
			theStack.pane = theStack.emptyPane
		}
	}

    }//end of column with padding


	Connections {
		target: gcd

		onSupplyPeerSettings: function(onion, nick) {
			toolbar.text = nick
			txtOnion.text = onion
			txtDisplayName.text = nick
		}
	}
}