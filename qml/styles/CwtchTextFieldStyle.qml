import QtQuick.Controls.Styles 1.4
import QtQuick 2.7


TextFieldStyle {
    id: root
	textColor: "black"
	font.pointSize: 10
	property int width: 100

	background: Rectangle {
		radius: 2
		implicitWidth: root.width
		implicitHeight: 24
		color: windowItem.cwtch_background_color
		border.color: windowItem.cwtch_color
	}
}