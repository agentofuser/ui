import QtQuick.Controls.Styles 1.4
import QtQuick 2.7

ButtonStyle {
	background: Rectangle {
			width:25
			height:25
			color: windowItem.cwtch_dark_color
			border.color: windowItem.cwtch_color
	}

	label: Text {
		renderType: Text.NativeRendering
		verticalAlignment: Text.AlignVCenter
		horizontalAlignment: Text.AlignHCenter
		font.family: "Monospace"
		font.pointSize: 8
		color: windowItem.cwtch_background_color
		text: control.text
	}
}