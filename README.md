# Cwtch - UI

This codebase provides a graphical user interface for Desktop and Android for [Cwtch: Privacy Preserving Infrastructure for Asynchronous, Decentralized and Metadata Resistant Applications](https://git.openprivacy.ca/cwtch.im/cwtch)

# Security

**Cwtch is an experimental concept and prototype. We do not recommend you use Cwtch today if you require secure communication.** At least, not yet.
 
 If you discover a security issue, please log an issue above, or email team@cwtch.im.

# Compiling & Running
This code relies on [therecipe/qt](https://github.com/therecipe/qt) before getting started consult the [Installation](https://github.com/therecipe/qt/wiki/Installation) and [Getting Started](https://github.com/therecipe/qt/wiki/Getting-Started) documentation to get that up and running. It will make building this much easier.

## Desktop
    qtdeploy build desktop
    ./deploy/linux/ui.sh -local -debug 2>&1 | grep -v 'Detected anchors on an item that is managed by a layout.'


The -local and -debug flags are helpful when developing.

## Android

We supply an arm-pie version of tor in `android/libs/armeabi-v7a` with the name `libtor.so`

    qtdeploy -docker build android
    adb install deploy/android/build-debug.apk

## Windows

- Download and install [QT](https://www.qt.io/download) Open Source edition
- Go to My Computer - Right Click on This PC - Advanced System Settings -  Environment Variables
    - New - QT_DIR = C:\Qt (or wherever you have it installed)
    - New - QT_VERSION = 5.12.1 (because I picked 5.12.1)
    - Edit the Path Variable and Add C:\Qt\Tools\mingw530_32\bin
- Install [MSYS2](https://www.msys2.org/) (Bash shell for windows)
- Install [Go](https://golang.org/) and make sure it's in the PATH, and add your GOPATH/bin to the PATH
- Setup therecipe/qt (instructions https://github.com/therecipe/qt/wiki/Installation-on-Windows)
    - `go get -u -v github.com/therecipe/qt/cmd/...`
    - `qtsetup` (Can take around an hour?)

If all that is done, then check out cwtch.im/ui and run `qtdeploy` in the root of it.

Currently you will need to manually run tor first before running cwtch.im/ui. Use the included tor and torrc file: `tor -f torrc`

# Development

## Translations

Translations can be update by runing QT Linguist in the project directory (`linguist`) and loading and editing any of the `i18n/translate_*.ts` files.  To add a new translation, please register the file in qml.qrc. To update translations, run `lupdate ui.pro` to update the editable .ts files and when you are done run `lrelease ui.pro` to generate the .qm files that are used at runtime.
